terraform {
  required_version = ">= 1.1"

  required_providers {
    aws = {
      source  = "hashicorp/aws"
      version = "~> 5.0"
    }
  }
}

resource "aws_instance" "this" {
    ami = var.ami
    availability_zone = var.az
    instance_type = var.instance_type
    tags = {
        Name = "server1"
    } 
  
}

resource "aws_instance" "second_server" {
    ami = var.ami
    availability_zone = var.az
    instance_type = var.instance_type
    tags = {
        Name = "server2"
    } 
  
}